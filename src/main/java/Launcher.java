import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;

public class Launcher {

    public static void main(String[] args) throws InterruptedException {

        ///////////// Part one: ///////////////

        String name = "webinar.test@gmail.com";
        String pass = "Xcg7299bnSmMuRLp9ITw";
        String address = "http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/";


        //Initiate driver
        WebDriver driver = initChromeDriver();

        //open page
        driver.get(address);

        //enter E-mail
        driver.findElement(By.name("email")).clear();
        driver.findElement(By.name("email")).sendKeys(name);

        //enter password
        driver.findElement(By.name("passwd")).clear();
        driver.findElement(By.name("passwd")).sendKeys(pass);

        //click button
        driver.findElement(By.name("submitLogin")).click();
        Thread.sleep(1000);

        //click user profile
        driver.findElement(By.id("employee_infos")).click();

        //Click logout
        driver.findElement(By.id("header_logout")).click();
        Thread.sleep(1000);


        ///////////// Part two: ///////////////

        //Login into the application

        driver.findElement(By.name("email")).clear();
        driver.findElement(By.name("email")).sendKeys(name);

        //enter password
        driver.findElement(By.name("passwd")).clear();
        driver.findElement(By.name("passwd")).sendKeys(pass);

        //Click Login button
        driver.findElement(By.name("submitLogin")).click();
        Thread.sleep(1000);

        // Create list of items in menu
        ArrayList<String> list = new ArrayList<String>();

        list.add("Dashboard");
        list.add("Заказы");
        list.add("Каталог");
        list.add("Клиенты");
        list.add("Служба поддержки");
        list.add("Статистика");
        list.add("Modules");
        list.add("Design");
        list.add("Доставка");
        list.add("Способ оплаты");
        list.add("International");
        list.add("Shop Parameters");
        list.add("Конфигурация");

        //Loop menu items
        for (String l : list) {

            Thread.sleep(1000);
            System.out.println("Menu item: " + l);

            //Click each menu item from list above
            driver.findElement(By.xpath("//ul//span[contains(text(),'"+l+"')]")).click();
            Thread.sleep(3000);

            //Get title before refresh
            System.out.println("Page title befor refresh: "+ driver.findElement(By.xpath("//h2[@class='page-title'|@class='title']")).getText());

            //Refresh page
            driver.navigate().refresh();
            Thread.sleep(2000);

            //Get title after refresh
            System.out.println("Page title after refresh: "+ driver.findElement(By.xpath("//h2[@class='page-title'|@class='title']")).getText());
            System.out.println("-------------//------------");
        }


        driver.quit();


    }


    private static WebDriver initChromeDriver() {
        //I have both macos and windows so I use two versions of driver

        String OS = System.getProperty("os.name").toLowerCase();
        if (OS.startsWith("win")) {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
        }
        if (OS.startsWith("mac")) {
            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver");
        }
        return new ChromeDriver();
    }


}
